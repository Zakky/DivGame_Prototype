﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// ゲーム全体の動きを管理する
/// </summary>
public class GameManager : MonoBehaviour {

	public GameObject Scores; // スコア表記のオブジェクト
	public int Score;					// スコア(敵を倒すと１点)

	// 数字ボタンをクリックしたか
	private bool isNumberClicked = false;
	public bool IsNumberClicked{
		get{
			return isNumberClicked;
		}
		set{
			isNumberClicked = value;
		}
	}

	public GameObject[] EnemySpawner; // 敵が湧くポイントのオブジェクト

	// クリックした数字
	private int clickedNumber;
	public int ClickedNumber{
		get{
			return clickedNumber;
		}
		set{
			clickedNumber = value;
		}
	}

	// はじめに保存してあるハイスコアを表示させる
	void Start () {
		Scores.transform.FindChild ("HighScore").GetComponent<Text> ().text = PlayerPrefs.GetInt("HighScore", 0).ToString();

		Score = 0;
	}
	
	// Update is called once per frame
	void Update () {
		Scores.transform.FindChild ("Score").GetComponent<Text> ().text = Score.ToString();

		// ランダムのタイミングで何処かの出現場所から敵が出現するようにする
		int rand = Random.Range (0, 100);
		if(rand > 90){
			int spawnNumber = Random.Range (0, EnemySpawner.Length);
			var esManager = EnemySpawner [spawnNumber].GetComponent<EnemySpawnController> ();
			if(!esManager.IsSpawned){
				esManager.Spawn ();
			}
		}
	}

	/// <summary>
	/// ハイスコアを保存する
	/// </summary>
	public void SaveHighScore(){
		if (PlayerPrefs.GetInt ("HighScore", 0) < Score){
			PlayerPrefs.SetInt ("HighScore", Score);
		}
	}
}
