﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 敵の出現を管理する
/// </summary>
/// <remarks>
/// コピー元のオブジェクトを指定してそれをCloneすることで出現する流れ
/// </remarks>
public class EnemySpawnController : MonoBehaviour {
	public GameObject Enemy;			// コピー元のオブジェクト
	public bool IsSpawned = false;	// 現在この出現場所から敵が出現しているか

	/// <summary>
	/// コピー元のオブジェクトをCloneして敵を出現させる
	/// </summary>
	public void Spawn(){
		IsSpawned = true;
		var clone = GameObject.Instantiate (Enemy) as GameObject;
		clone.SetActive (true);
		clone.GetComponent<EnemyController> ().Spawner = gameObject;
		clone.transform.parent = gameObject.transform.parent;
		clone.transform.localPosition = gameObject.transform.localPosition;
		clone.GetComponent<EnemyController> ().MoveEnemy ();
	}
}
