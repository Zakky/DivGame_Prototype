﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

/// <summary>
/// 下のボタンをクリックしたときの処理
/// </summary>
public class NumberButtonController : MonoBehaviour, IPointerClickHandler {
	public int Number; // 設定した数字
	public GameObject GameManagerObject;

	#region IPointerClickHandler implementation

	public void OnPointerClick (PointerEventData eventData)
	{
		GameManagerObject.GetComponent<GameManager> ().ClickedNumber = Number;
		GameManagerObject.GetComponent<GameManager> ().IsNumberClicked = true;
		Debug.Log ("Click : " + Number);
	}

	#endregion
}
