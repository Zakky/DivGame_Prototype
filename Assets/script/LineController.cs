﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// GameOverになるラインの処理
/// </summary>
public class LineController : MonoBehaviour {
	[SerializeField]
	GameObject gameOverSplash;	// ゲームオーバー画面のオブジェクト

	public GameObject GameManagerObject;

	// 敵がラインに到達したときの処理
	void OnTriggerEnter2D(Collider2D collider){
		Destroy (collider.gameObject);
		GameManagerObject.GetComponent<GameManager> ().SaveHighScore ();
		gameOverSplash.SetActive (true);
		StartCoroutine ("ReloadScene");
	}

	/// <summary>
	/// ステージをリロードする
	/// </summary>
	IEnumerator ReloadScene(){
		Debug.Log("Reload!");
		yield return new WaitForSeconds (2);		// 2秒待つ 
		SceneManager.LoadScene (SceneManager.GetActiveScene ().name);
	}
}
