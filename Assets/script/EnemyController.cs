﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

/// <summary>
/// 敵の動きを管理する
/// </summary>
public class EnemyController : MonoBehaviour, IPointerClickHandler {

	public GameObject GameManagerObject;
	private GameManager gameManager;

	public float Speed;					// スピード
	private float adjast = 1000;		// 調整値

	private GameObject Text;
	private int number;

	// 出現場所のオブジェクト
	private GameObject spawner;
	private EnemySpawnController esController;
	public GameObject Spawner{
		set{
			spawner = value;
		}
	}
		
	//出現時の処理
	void Start () {
		
		Text = transform.FindChild ("Canvas").FindChild ("Text").gameObject;
		number = Random.Range (2, 50);
		Text.GetComponent<Text> ().text = number.ToString ();

		// 後々呼び出しやすいように
		gameManager = GameManagerObject.GetComponent<GameManager> ();
		esController = spawner.GetComponent<EnemySpawnController> ();
	}
		
	#region IPointerClickHandler implementation

	/// <summary>
	/// クリックしたときの処理
	/// </summary>
	/// <param name="eventData">Event data.</param>
	/// <remarks>
	/// 敵をクリックする前に数字ボタンをクリックしていた場合、その数字と敵が持つ数字によって処理を行う
	/// 敵の数字が1になったら or 2桁の素数のときに素数ボタンで消える
	/// </remarks>
	public void OnPointerClick (PointerEventData eventData)
	{
		int divNumber = gameManager.ClickedNumber;
		if(gameManager.IsNumberClicked){
			// Xボタン(素数ボタン)をクリックしたときの処理
			if(divNumber == -1){
				if(number >= 11 && IsPrime(number)){
					gameManager.Score++;
					gameManager.IsNumberClicked = false;
					esController.IsSpawned = false;
					Destroy (gameObject);
				}
			}
			else if(number % divNumber == 0){
				number /= divNumber;
				gameManager.IsNumberClicked = false;
				if(number == 1){
					gameManager.Score++;
					esController.IsSpawned = false;
					Destroy (gameObject);
				}
				Text.GetComponent<Text> ().text = number.ToString ();
			}
		}
	}

	#endregion

	/// <summary>
	/// 素数かどうかを判定するメソッド
	/// </summary>
	/// <returns><c>true</c> 素数 <c>false</c>まだ割れる</returns>
	/// <param name="n">整数N</param>
	private bool IsPrime(int n){
		if (n < 2){
			return false;
		}
		if(n == 2){
			return true;
		}

		for(int i = 2; i < n; i++){
			if(n % i == 0){
				return false;
			}
		}
		return true;
	}

	/// <summary>
	/// 移動させる
	/// </summary>
	public void MoveEnemy(){
		gameObject.GetComponent<Rigidbody2D> ().AddForce (Vector2.down * (Speed / adjast), ForceMode2D.Force);
	}
}
